# the-better-grep-search

## What's the problem?
For us who do a lot of stuff in command line, we often do text search via `grep`. I really like
`grep`. But the problem is that these tools were designed in the last century and there are certain
aspects of them that make us frown our eyebrows:

- They have crazy amount of configurations, just do `man grep` and see for yourself
- They default to search for everything, meaning they search built/minified code, library code, svg
  etc., and you have to manually exclude files types/file paths every time
- The output is not very IDE friendly, as a vim user, it's hard for me to step in and press a few
  keys and just open that file in vim. I assume for other IDEs the frustration remains

## Why not use ack?
Now I understand `ack` is better, other than the IDE friendliness aspect, `ack` overcomes all the
shortcomings of `grep` I mentioned above, but hey it's hackathon, let's write our new tool!

## How to run?
**[IMPORTANT] Currently only MacOS is supported.** Linux Mint support is coming in a few weeks (as
of 2020/05/02), but don't expect much more than that.

First you git clone this repo.

#### If you use fish shell
- Install tbgs by running this once: `fish install.fish`
- Afterwards you can run `tbgs` in your fish shell

#### If you use bash shell
- Install tbgs by running this once: `bash install.bash`
- Afterwards you can run `tbgs` in your bash shell

## How to run tests?
Simply cd to the repository root dir, and run `bats tests/**/*.bats`.

Be sure to have bats installed, it's trivial with `Homebrew` or `apt`.

## Quick starters
Visit my asciinema: <https://asciinema.org/~kirkkt>

## keywords

| keyword  | meaning | default value | example and notes | demo |
|----------|---------|---------------|---------|---|
| `color`   | use highlight colors for matches  | ✅           |  `tbgs color Star`      | <https://asciinema.org/a/295057> |
| `no-color`| do not use highlight colors | -     |  `tbgs no-color Star`        | " |
|||||
| `case`   | case sensitive   |      -        |  `tbgs case Star` will not match `star` and `STAR`      | <https://asciinema.org/a/295055> |
| `no-case`| case insensitive        | ✅             |  `tbgs no-case Star` will match `star` and `STAR`        | "|
|||||
| `pipe-to`| specify where to pipe grep result |  `none`             | `tbgs Star pipe-to none`, also accepting: `vim`, `less` and external programs (see notes below) |<https://asciinema.org/a/326298>|
|||||
| `word`| respect word boundary        |  ✅             |  `tbgs word star` will not match `starcraft`, `stare` and `costar`        |<https://asciinema.org/a/295059>|
| `no-word`| ignore word boundary        |  -             |  `tbgs no-word star` will match `starcraft`, `stare` and `costar`      |"|
|||||
| `around`| show around lines, followed by an integer denoting the number of lines |  0      |  `tbgs around 2 star` will also show 2 lines above and below per match        |<https://asciinema.org/a/295056>|
|||||
| `recur`| search in files recursively (including all descendant directories)       |  ✅             |  `tbgs recur star` will match `star`, if it's located in `./path/to/file`        | <https://asciinema.org/a/295058>|
| `no-recur`| search in fiels non-recursively (only search among files in the current directory)       |  -             |  `tbgs no-recur star` will not match `star`, if it's located in `./path/to/file`      |"|
|||||
| `no-dot`| do not search for dot files | - | By default, tbgs will search for both normal and dot files and directories. Use `tbgs star no-dot` to only search normal files under normal directory | <https://asciinema.org/a/324419> |
|||||
| `keyword`| explicitly specify search word (tells tbgs to not treat the next string as a special keyword), followed by a string |  -             |  `tbgs keyword color` will search `color` as a normal string, `tbgs keyword color keyword recur` will do multi-search on words `color` and `recur`  | <https://asciinema.org/a/295065>|
|||||
| `max-length`| max length of the line, followed by an integer denoting this length. |  1000             |  `tbgs max-length 500 star` will not match a line containing `star`, if that line exceeds 500 characters. **Note that this is not very precise.** Use it for big numbers like the thousands. **0 means to match lines with any length**  | <https://asciinema.org/a/295062>|

## pipe-to
tbgs defaults to output to stdout, but it is more powerful when the result is piped to elsewhere.
Below are the possible values of the `pipe-to` keyword:

- `none`
    - default value
    - simply output to stdout
    - e.g. `tbgs star pipe-to none`
- `less`
    - pipe to `less`
    - e.g. `tbgs star pipe-to less`
- `vim`
    - pipe to `vim`
    - press <space>o to open the file on the current line in `vim`
    - press <space>O to open all files in `vim`
    - e.g. `tbgs star pipe-to vim`
- `code`
    - pipe to `vim`
    - press <space>o to open the file on the current line in `Visual Studio Code`
    - press <space>O to open all files in `Visual Studio Code`
    - e.g. `tbgs star pipe-to code`
- `mvim`
    - pipe to `vim`
    - press <space>o to open the file on the current line in `MacVim`
    - press <space>O to open all files in `MacVim`
    - e.g. `tbgs star pipe-to mvim`
- `subl`
    - pipe to `vim`
    - press <space>o to open the file on the current line in `Sublime Text`
    - press <space>O to open all files in `Sublime Text`
    - e.g. `tbgs star pipe-to subl`
- `chrome`
    - pipe to `vim`
    - press <space>o to open the file on the current line in `Google Chrome`
    - press <space>O to open all files in `Google Chrome`
    - e.g. `tbgs star pipe-to chrome`
- `canary`
    - pipe to `vim`
    - press <space>o to open the file on the current line in `Google Chrome Canary`
    - press <space>O to open all files in `Google Chrome Canary`
    - e.g. `tbgs star pipe-to canary`
- Any value
    - pipe to `vim`
    - uses `open -a {external program name} {file name}` command on MacOS
    - press <space>o to open the file on the current line in the external program
    - press <space>O to open all files in the external program
    - e.g. `tbgs star pipe-to Xcode` or `tbgs star pipe-to 'Visual Studio Code'`
    - make sure you handle space properly by either escaping it `\ ` or by using quotation marks

## Multi-search
tbgs accepts multiple search words, and it will search for lines where all the search words match. Demo: <https://asciinema.org/a/295054>

## Configuration files
You can specify a `.tbgs-config.json` file in a directory, where you can redefine default settings.

You can also specify a **unique** user configuration file in `~/.tbgs-config.json`.

User configuration file overwrites the default system configuration file (located at `./config.json`
in this repo). Local configuration files overwrite user configuration file. Lower level local
configuration files overwrite higher level ones. See the diagram below:

      +---- ./config.json in this repo => system configuration file
      |
      +-------- ~/.tbgs-config.json => user configuration file
      |
      +------------ ../../.tbgs-config.json => local configuration file at grandparent directory
      |
      +---------------- ./.tbgs-config.json => local configuration file at working directory

In this example, if:

1. in `./.tbgs-config.json`, the value of `pipeTo` is `"vim"`, and in `../../.tbgs-config.json`, the
   value of `pipeTo` is `"less"`, then `tbgs star` will pipe to vim. **In other words, lower level
   configuration wins in a key conflict.**
2. in both `./.tbgs-config.json` and `../../.tbgs-config.json`, the key `excludeFiles` is not
   defined, in `~/.tbgs-config.json`, the value of `excludeFiles` is `[ "a", "b" ]`, and in the
   system configuration file, the value of `excludeFiles` is `[ "c" ]`, then `tbgs star` will
   exclude file `a` and `b`, but will not exclude file `c`. **In other words, omitted properties get
   passed down hierarchically.**

All the available attributes can be found at `./config.json` in this repo.

See <https://asciinema.org/a/295064> for a quick demo.

## maps
Once you are familiar with tbgs, you might find the need to have some shortcuts for some of the
common search keywords you use. You don't have to do this, but it saves a few key strokes.

You can specify maps inside various configuration files, e.g.:

```
{
  "pipeTo": "vim",
  "maps": {
    "pn": "pipe-to none",
    "pl": "pipe-to less",
    "pv": "pipe-to vim",
    "wc": "word color"
  }
}
```

Four maps are created here. With that, the followings are equivalent:

- `tbgs pn wc my_search_string`
- `tbgs pipe-to none word case my_search_string`

Demo: <https://asciinema.org/a/361209>

### But what if I indeed want to search for one of the map keys?
Assume we are still using the previous configuration file, if you indeed want to search for `pn`,
you can simply prefix it with `keyword`, like this:

```
tbgs keyword pn
```

This works because the word after `keyword` is always treated as a normal string.
