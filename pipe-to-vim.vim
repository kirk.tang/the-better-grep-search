" opens all files in the current buffer and closes the grep result buffer
function! OpenAllFilesInVimFunction()
  g/:\d*:/normal nD
  sort u
  %s/\(.*\)/e \1
  %d b
  bd!
  @b
  silent! unmap <cr>
  next
endfunction

function! OpenCurrentFileInVimFunction()
  normal 0"udt:u
  execute 'edit' @u
endfunction

" mappings
nnoremap <space>o :silent! call OpenCurrentFileInVimFunction()<cr>
nnoremap <space>O :silent! call OpenAllFilesInVimFunction()<cr>
