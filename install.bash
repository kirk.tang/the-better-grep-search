#!/bin/bash

install_tbgs_bash() {
  if [ "$(uname)" == "Darwin" ]; then
    echo ". "$( realpath $( dirname "${BASH_SOURCE[0]}" ) )"/tbgs.bash" >> ~/.bash_profile
  else
    echo ". "$( realpath $( dirname "${BASH_SOURCE[0]}" ) )"/tbgs.bash" >> ~/.bashrc
  fi
  echo "tbgs installation done, please start a new bash session by running \`bash\`"
}

command -v tbgs >/dev/null 2>&1 || { install_tbgs_bash; exit 1; }
echo 'You already have tgbs, cheers!'
exit 0
