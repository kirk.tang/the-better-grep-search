#!/usr/bin/env bats

@test "static two exclude-files with exclude pattern: static" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, four
file1:38:One, two, three, four
file1:55:One, two, three, four
file1:63:One, two, three, four
fileDir2/file2:3:for loop that C-based languages
fileDir2/file2:4:  actually foreach loops.
fileDir2/file2:8:problems with for loops in"
  result="$(fish -c 'tbgs fo')"
  [ "$result" = "$expected" ]
}
