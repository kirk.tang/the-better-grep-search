#!/usr/bin/env bats

@test "filter around no-color case: 2 words" {
  cd $BATS_TEST_DIRNAME
  expected="file11-16-More junk in the trunk than a Honda
file11-17-I know you wanna do the Jane Fonda
file11:18:One, two, three, four
file11-19-Get your booty on the dance floor
file11-20-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs One Honda')"; [ "$result" = "$expected" ]
  expected="file11-16-More junk in the trunk than a Honda
file11-17-I know you wanna do the Jane Fonda
file11:18:One, two, three, four
file11-19-Get your booty on the dance floor
file11-20-Work it out, shake it, little mama
--
--
file11-36-She took me home to her mama
file11-37-I taught them both how to Jane Fonda
file11:38:One, two, three, four
file11-39-Get your booty on the dance floor
file11-40-Work it out, shake it, little mama
--
--
file11-61-To work it out; work it, little mama
file11-62-I know you wanna do the Jane Fonda
file11:63:One, two, three, four
file11-64-Get your booty on the dance floor
file11-65-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs One Fonda')"; [ "$result" = "$expected" ]
  expected=""
  result="$(fish -c 'tbgs One fonda')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs One honda')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs one Fonda')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs one Honda')"; [ "$result" = "$expected" ]
}

@test "filter around no-color case: 4 words" {
  cd $BATS_TEST_DIRNAME
  expected="file11-16-More junk in the trunk than a Honda
file11-17-I know you wanna do the Jane Fonda
file11:18:One, two, three, four
file11-19-Get your booty on the dance floor
file11-20-Work it out, shake it, little mama
--
--
file11-61-To work it out; work it, little mama
file11-62-I know you wanna do the Jane Fonda
file11:63:One, two, three, four
file11-64-Get your booty on the dance floor
file11-65-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs two One dance know')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two dance One know')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs One two know dance')"; [ "$result" = "$expected" ]
  expected="file11-17-I know you wanna do the Jane Fonda
file11-18-One, two, three, four
file11:19:Get your booty on the dance floor
file11-20-Work it out, shake it, little mama
file11-21-Let me see you do the Jane Fonda
--
--
file11-62-I know you wanna do the Jane Fonda
file11-63-One, two, three, four
file11:64:Get your booty on the dance floor
file11-65-Work it out, shake it, little mama
file11-66-Let me see you do the Jane Fonda"
  result="$(fish -c 'tbgs dance two know One')"; [ "$result" = "$expected" ]
  expected=""
  result="$(fish -c 'tbgs two one dance know')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two dance one know')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs one two know dance')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs dance two know one')"; [ "$result" = "$expected" ]
}

@test "filter around no-color case: 2 words partial match" {
  cd $BATS_TEST_DIRNAME
  expected="file11-16-More junk in the trunk than a Honda
file11-17-I know you wanna do the Jane Fonda
file11:18:One, two, three, four
file11-19-Get your booty on the dance floor
file11-20-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs One Hond')"; [ "$result" = "$expected" ]
  expected="file11-16-More junk in the trunk than a Honda
file11-17-I know you wanna do the Jane Fonda
file11:18:One, two, three, four
file11-19-Get your booty on the dance floor
file11-20-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs On Hond')"; [ "$result" = "$expected" ]
  expected="file11-14-Licky, licky, sucky, sucky
file11-15-Mickey, Mickey, fuck me, fuck me
file11:16:More junk in the trunk than a Honda
file11:17:I know you wanna do the Jane Fonda
file11-18-One, two, three, four"
  result="$(fish -c 'tbgs on Hond')"; [ "$result" = "$expected" ]
  expected=""
  result="$(fish -c 'tbgs one Hond')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs One hond')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs On hond')"; [ "$result" = "$expected" ]
}

@test "filter around no-color case: 2 words word only appears in file path" {
  cd $BATS_TEST_DIRNAME
  expected=""
  result="$(fish -c 'tbgs one file')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs file two')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs One file')"; [ "$result" = "$expected" ]
}
