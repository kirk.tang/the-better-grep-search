#!/usr/bin/env bats

@test "static color: static" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, [01;31m[Kfo[m[Kur
file1:38:One, two, three, [01;31m[Kfo[m[Kur
file1:55:One, two, three, [01;31m[Kfo[m[Kur
file1:63:One, two, three, [01;31m[Kfo[m[Kur"
  result="$(fish -c 'tbgs fo')"
  [ "$result" = "$expected" ]
}

@test "static color: when dynamic is the same as static, nothing happens" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, [01;31m[Kfo[m[Kur
file1:38:One, two, three, [01;31m[Kfo[m[Kur
file1:55:One, two, three, [01;31m[Kfo[m[Kur
file1:63:One, two, three, [01;31m[Kfo[m[Kur"
  result="$(fish -c 'tbgs color fo')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs fo color')"
  [ "$result" = "$expected" ]
}

@test "static color: dynamic overwrites static" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, four
file1:38:One, two, three, four
file1:55:One, two, three, four
file1:63:One, two, three, four"
  result="$(fish -c 'tbgs no-color fo')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs fo no-color')"
  [ "$result" = "$expected" ]
}
