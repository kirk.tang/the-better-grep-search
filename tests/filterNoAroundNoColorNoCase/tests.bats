#!/usr/bin/env bats

@test "filter no-around no-color no-case: 2 words" {
  cd $BATS_TEST_DIRNAME
  expected="file11:18:One, two, three, four
file11:38:One, two, three, four
file11:55:One, two, three, four
file11:63:One, two, three, four"
  result="$(fish -c 'tbgs one two')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two one')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs one two no-case')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two one no-case')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs one no-case two')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two no-case one')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs no-case one two')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs no-case two one')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs one two no-color')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two one no-color')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs no-case one two no-color')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two one no-case no-color')"; [ "$result" = "$expected" ]
  expected="file21:3:Her debut single, a cover version of Bon Iver's Skinny Love, was her breakthrough, charting all across Europe and earning platinum certification six times in Australia. Her self-titled debut album, Birdy, was released on 7 November 2011 to similar success, peaking at number one in Australia, Belgium and the Netherlands. Her second studio album, Fire Within, was released on 23 September 2013 in the UK. At the 2014 Brit Awards, she was nominated for Best British Female Solo Artist. Her third studio album, Beautiful Lies, was released on 25 March 2016."
  result="$(fish -c 'tbgs one uk')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs uk one')"; [ "$result" = "$expected" ]
}

@test "filter no-around no-color no-case: 4 words" {
  cd $BATS_TEST_DIRNAME
  expected="file11:18:One, two, three, four
file11:38:One, two, three, four
file11:55:One, two, three, four
file11:63:One, two, three, four"
  result="$(fish -c 'tbgs one two four three')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two one three four')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs one two four no-color three')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two one three four no-case')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs no-color no-case one two four three')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs no-case two one no-color three four')"; [ "$result" = "$expected" ]
  expected=""
  result="$(fish -c 'tbgs one uk four three')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs uk one three uk')"; [ "$result" = "$expected" ]
}

@test "filter no-around no-color no-case: 2 words partial match" {
  cd $BATS_TEST_DIRNAME
  expected="file11:18:One, two, three, four
file11:38:One, two, three, four
file11:55:One, two, three, four
file11:63:One, two, three, four"
  result="$(fish -c 'tbgs one tw')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two on')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs thr wo')"; [ "$result" = "$expected" ]
expected="file11:18:One, two, three, four
file11:38:One, two, three, four
file11:55:One, two, three, four
file11:63:One, two, three, four
file21:3:Her debut single, a cover version of Bon Iver's Skinny Love, was her breakthrough, charting all across Europe and earning platinum certification six times in Australia. Her self-titled debut album, Birdy, was released on 7 November 2011 to similar success, peaking at number one in Australia, Belgium and the Netherlands. Her second studio album, Fire Within, was released on 23 September 2013 in the UK. At the 2014 Brit Awards, she was nominated for Best British Female Solo Artist. Her third studio album, Beautiful Lies, was released on 25 March 2016."
  result="$(fish -c 'tbgs thr w')"; [ "$result" = "$expected" ]
}

@test "filter no-around no-color no-case: 2 words word only appears in file path" {
  cd $BATS_TEST_DIRNAME
  expected=""
  result="$(fish -c 'tbgs one file')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs file two')"; [ "$result" = "$expected" ]
}
