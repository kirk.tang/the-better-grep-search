#!/usr/bin/env bats

@test "static maps: maps work" {
  cd $BATS_TEST_DIRNAME
  expected="file1:16:More junk in the trunk than a Honda
file1:27:She was in bred Jean had big breasts?
file1:50:Like Janet Jackson in Rhythm Nation"
  result="$(fish -c 'tbgs in w')"; [ "$result" = "$expected" ]
  expected="file1:29:Jean wasn't fast, she was easy to catch
file1:31:So fly she was Trans-Atlantic"
  result="$(fish -c 'tbgs she wc')"; [ "$result" = "$expected" ]
}

@test "static maps: keyword can escape maps" {
  cd $BATS_TEST_DIRNAME
  expected="There is no search word to search!"
  result="$(fish -c 'tbgs her')"; [ "$result" = "$expected" ]
  expected="file1:3:Said her Daddy used to hang
file1:6:With her friend named Jen
file1:7:Her booty was bigger than a Mercedes Benz
file1:8:Jen was a herdy-gerdy-dirty little girly
file1:36:She took me home to her mama
file1:49:She was slammin', and her ass was jammin'
file1:51:Her brother Jason had a girl named Grace
file1:52:You could see her ass from outer space"
  result="$(fish -c 'tbgs keyword her')"; [ "$result" = "$expected" ]
  expected="file1:3:Said her Daddy used to hang
file1:6:With her friend named Jen
file1:36:She took me home to her mama
file1:49:She was slammin', and her ass was jammin'
file1:52:You could see her ass from outer space"
  result="$(fish -c 'tbgs keyword her her')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs her keyword her')"; [ "$result" = "$expected" ]
}
