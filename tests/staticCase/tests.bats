#!/usr/bin/env bats

@test "static case: static" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, four
file1:38:One, two, three, four
file1:55:One, two, three, four
file1:63:One, two, three, four"
  result="$(fish -c 'tbgs fo')"
  [ "$result" = "$expected" ]
}

@test "static case: when dynamic is the same as static, nothing happens" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, four
file1:38:One, two, three, four
file1:55:One, two, three, four
file1:63:One, two, three, four"
  result="$(fish -c 'tbgs case fo')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs fo case')"
  [ "$result" = "$expected" ]
}

@test "static case: dynamic overwrites static" {
  cd $BATS_TEST_DIRNAME
  expected="file1:17:I know you wanna do the Jane Fonda
file1:18:One, two, three, four
file1:21:Let me see you do the Jane Fonda
file1:25:I know you wanna do the Jane Fonda
file1:37:I taught them both how to Jane Fonda
file1:38:One, two, three, four
file1:41:Let me see you do the Jane Fonda
file1:45:I know you wanna do the Jane Fonda
file1:55:One, two, three, four
file1:58:Let me see you do the Jane Fonda
file1:62:I know you wanna do the Jane Fonda
file1:63:One, two, three, four
file1:66:Let me see you do the Jane Fonda
file1:70:I know you wanna do the Jane Fonda"
  result="$(fish -c 'tbgs no-case fo')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs fo no-case')"
  [ "$result" = "$expected" ]
}
