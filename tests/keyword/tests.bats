#!/usr/bin/env bats

@test "keyword: keyword should not be searched by default" {
  cd $BATS_TEST_DIRNAME
  expected="There is no search word to search!"
  result="$(fish -c 'tbgs color case')"
  [ "$result" = "$expected" ]
}

@test "keyword: one keyword" {
  cd $BATS_TEST_DIRNAME
  expected="file11:70:I know you wanna do the Jane Fonda color"
  result="$(fish -c 'tbgs keyword color case')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs case keyword color')"
  [ "$result" = "$expected" ]
}

@test "keyword: old pattern should no longer be a keyword" {
  cd $BATS_TEST_DIRNAME
  expected="file11:71:pattern"
  result="$(fish -c 'tbgs pattern')"
  [ "$result" = "$expected" ]
}

@test "keyword: new no-dot should be a keyword" {
  cd $BATS_TEST_DIRNAME
  expected="There is no search word to search!"
  result="$(fish -c 'tbgs no-dot')"
  [ "$result" = "$expected" ]
  expected="file11:72:no-dot"
  result="$(fish -c 'tbgs keyword no-dot')"
  [ "$result" = "$expected" ]
}
