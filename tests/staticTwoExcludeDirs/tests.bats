#!/usr/bin/env bats

@test "static two exclude-dirs: static" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, four
file1:38:One, two, three, four
file1:55:One, two, three, four
file1:63:One, two, three, four"
  result="$(fish -c 'tbgs fo')"
  [ "$result" = "$expected" ]
}
