#!/usr/bin/env bats

@test "no-dot: searches both normal and dot files by default" {
  cd $BATS_TEST_DIRNAME
  expected="file11:9:I heard it from a birdy
.dir1/file11:9:I heard it from a birdy
.dir1/.file21:1:Jasmine Lucilla Elizabeth Jennifer van den Bogaerde[5] (born 15 May 1996), better known by her stage name Birdy, is an English singer, songwriter and musician. She won the music competition Open Mic UK in 2008, at the age of 12.
.dir1/.file21:3:Her debut single, a cover version of Bon Iver's Skinny Love, was her breakthrough, charting all across Europe and earning platinum certification six times in Australia. Her self-titled debut album, Birdy, was released on 7 November 2011 to similar success, peaking at number one in Australia, Belgium and the Netherlands. Her second studio album, Fire Within, was released on 23 September 2013 in the UK. At the 2014 Brit Awards, she was nominated for Best British Female Solo Artist. Her third studio album, Beautiful Lies, was released on 25 March 2016.
.file21:1:Jasmine Lucilla Elizabeth Jennifer van den Bogaerde[5] (born 15 May 1996), better known by her stage name Birdy, is an English singer, songwriter and musician. She won the music competition Open Mic UK in 2008, at the age of 12.
.file21:3:Her debut single, a cover version of Bon Iver's Skinny Love, was her breakthrough, charting all across Europe and earning platinum certification six times in Australia. Her self-titled debut album, Birdy, was released on 7 November 2011 to similar success, peaking at number one in Australia, Belgium and the Netherlands. Her second studio album, Fire Within, was released on 23 September 2013 in the UK. At the 2014 Brit Awards, she was nominated for Best British Female Solo Artist. Her third studio album, Beautiful Lies, was released on 25 March 2016."
  result="$(fish -c 'tbgs birdy')"
  [ "$result" = "$expected" ]
}

@test "no-dot: dynamic overwrites static" {
  cd $BATS_TEST_DIRNAME
  expected="file11:9:I heard it from a birdy"
  result="$(fish -c 'tbgs birdy no-dot')"
  [ "$result" = "$expected" ]
}
