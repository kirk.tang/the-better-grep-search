#!/usr/bin/env bats

@test "static word: static" {
  cd $BATS_TEST_DIRNAME
  expected="fileDir2/file2:3:for loop that C-based languages
fileDir2/file2:8:problems with for loops in"
  result="$(fish -c 'tbgs for')"
  [ "$result" = "$expected" ]
}

@test "static word: when dynamic is the same as static, nothing happens" {
  cd $BATS_TEST_DIRNAME
  expected="fileDir2/file2:3:for loop that C-based languages
fileDir2/file2:8:problems with for loops in"
  result="$(fish -c 'tbgs word for')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs for word')"
  [ "$result" = "$expected" ]
}

@test "static word: dynamic overwrites static" {
  cd $BATS_TEST_DIRNAME
  expected="fileDir2/file2:3:for loop that C-based languages
fileDir2/file2:4:  actually foreach loops.
fileDir2/file2:8:problems with for loops in"
  result="$(fish -c 'tbgs no-word for')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs for no-word')"
  [ "$result" = "$expected" ]
}
