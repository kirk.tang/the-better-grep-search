#!/usr/bin/env bats

@test "filter around color no-case: 2 words" {
  cd $BATS_TEST_DIRNAME
  expected="file11-16-More junk in the trunk than a [0;32mHonda[0m
file11-17-I know you wanna do the Jane Fonda
file11:18:[01;31m[KOne[m[K, two, three, four
file11-19-Get your booty on the dance floor
file11-20-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs one honda')"; [ "$result" = "$expected" ]
  expected="file11-16-More junk in the trunk than a Honda
file11-17-I know you wanna do the Jane [0;32mFonda[0m
file11:18:[01;31m[KOne[m[K, two, three, four
file11-19-Get your booty on the dance floor
file11-20-Work it out, shake it, little mama
--
--
file11-36-She took me home to her mama
file11-37-I taught them both how to Jane [0;32mFonda[0m
file11:38:[01;31m[KOne[m[K, two, three, four
file11-39-Get your booty on the dance floor
file11-40-Work it out, shake it, little mama
--
--
file11-61-To work it out; work it, little mama
file11-62-I know you wanna do the Jane [0;32mFonda[0m
file11:63:[01;31m[KOne[m[K, two, three, four
file11-64-Get your booty on the dance floor
file11-65-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs one fonda')"; [ "$result" = "$expected" ]
}

@test "filter around color no-case: 4 words" {
  cd $BATS_TEST_DIRNAME
  expected="file11-16-More junk in the trunk than a Honda
file11-17-I [0;34mknow[0m you wanna do the Jane Fonda
file11:18:[0;32mOne[0m, [01;31m[Ktwo[m[K, three, four
file11-19-Get your booty on the [0;33mdance[0m floor
file11-20-Work it out, shake it, little mama
--
--
file11-61-To work it out; work it, little mama
file11-62-I [0;34mknow[0m you wanna do the Jane Fonda
file11:63:[0;32mOne[0m, [01;31m[Ktwo[m[K, three, four
file11-64-Get your booty on the [0;33mdance[0m floor
file11-65-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs two one dance know')"; [ "$result" = "$expected" ]
  expected="file11-16-More junk in the trunk than a Honda
file11-17-I [0;34mknow[0m you wanna do the Jane Fonda
file11:18:[0;33mOne[0m, [01;31m[Ktwo[m[K, three, four
file11-19-Get your booty on the [0;32mdance[0m floor
file11-20-Work it out, shake it, little mama
--
--
file11-61-To work it out; work it, little mama
file11-62-I [0;34mknow[0m you wanna do the Jane Fonda
file11:63:[0;33mOne[0m, [01;31m[Ktwo[m[K, three, four
file11-64-Get your booty on the [0;32mdance[0m floor
file11-65-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs two dance one know')"; [ "$result" = "$expected" ]
  expected="file11-16-More junk in the trunk than a Honda
file11-17-I [0;33mknow[0m you wanna do the Jane Fonda
file11:18:[01;31m[KOne[m[K, [0;32mtwo[0m, three, four
file11-19-Get your booty on the [0;34mdance[0m floor
file11-20-Work it out, shake it, little mama
--
--
file11-61-To work it out; work it, little mama
file11-62-I [0;33mknow[0m you wanna do the Jane Fonda
file11:63:[01;31m[KOne[m[K, [0;32mtwo[0m, three, four
file11-64-Get your booty on the [0;34mdance[0m floor
file11-65-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs one two know dance')"; [ "$result" = "$expected" ]
  expected="file11-17-I [0;33mknow[0m you wanna do the Jane Fonda
file11-18-[0;34mOne[0m, [0;32mtwo[0m, three, four
file11:19:Get your booty on the [01;31m[Kdance[m[K floor
file11-20-Work it out, shake it, little mama
file11-21-Let me see you do the Jane Fonda
--
--
file11-62-I [0;33mknow[0m you wanna do the Jane Fonda
file11-63-[0;34mOne[0m, [0;32mtwo[0m, three, four
file11:64:Get your booty on the [01;31m[Kdance[m[K floor
file11-65-Work it out, shake it, little mama
file11-66-Let me see you do the Jane Fonda"
  result="$(fish -c 'tbgs dance two know one')"; [ "$result" = "$expected" ]
}

@test "filter around color no-case: 2 words partial match" {
  cd $BATS_TEST_DIRNAME
  expected="file11-16-More junk in the trunk than a [0;32mHond[0ma
file11-17-I know you wanna do the Jane Fonda
file11:18:[01;31m[KOne[m[K, two, three, four
file11-19-Get your booty on the dance floor
file11-20-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs one hond')"; [ "$result" = "$expected" ]
  # fix bug and then re-enable
  # expected="file11-14-Licky, licky, sucky, sucky
# file11-15-Mickey, Mickey, fuck me, fuck me
# file11:16:More junk in the trunk than a Honda
# file11:17:I know you wanna do the Jane Fonda
# file11:18:One, two, three, four
# file11:19:Get your booty on the dance floor
# file11-20-Work it out, shake it, little mama"
  # result="$(fish -c 'tbgs on hond')"; [ "$result" = "$expected" ]
}

@test "filter around color no-case: 2 words word only appears in file path" {
  cd $BATS_TEST_DIRNAME
  expected=""
  result="$(fish -c 'tbgs one file')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs file two')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs One file')"; [ "$result" = "$expected" ]
}
