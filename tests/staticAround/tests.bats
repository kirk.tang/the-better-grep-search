#!/usr/bin/env bats

@test "static around: static" {
  cd $BATS_TEST_DIRNAME
  expected="file1-16-More junk in the trunk than a Honda
file1-17-I know you wanna do the Jane Fonda
file1:18:One, two, three, four
file1-19-Get your booty on the dance floor
file1-20-Work it out, shake it, little mama
--
--
file1-36-She took me home to her mama
file1-37-I taught them both how to Jane Fonda
file1:38:One, two, three, four
file1-39-Get your booty on the dance floor
file1-40-Work it out, shake it, little mama
--
--
file1-53-So I landed on a planet
file1-54-And planted a Mickey A flag, dammit
file1:55:One, two, three, four
file1-56-Get your booty on the dance floor
file1-57-Work it out, shake it, little mama
--
--
file1-61-To work it out; work it, little mama
file1-62-I know you wanna do the Jane Fonda
file1:63:One, two, three, four
file1-64-Get your booty on the dance floor
file1-65-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs fo')"
  [ "$result" = "$expected" ]
}

@test "static around: when dynamic is the same as static, nothing happens" {
  cd $BATS_TEST_DIRNAME
  expected="file1-16-More junk in the trunk than a Honda
file1-17-I know you wanna do the Jane Fonda
file1:18:One, two, three, four
file1-19-Get your booty on the dance floor
file1-20-Work it out, shake it, little mama
--
--
file1-36-She took me home to her mama
file1-37-I taught them both how to Jane Fonda
file1:38:One, two, three, four
file1-39-Get your booty on the dance floor
file1-40-Work it out, shake it, little mama
--
--
file1-53-So I landed on a planet
file1-54-And planted a Mickey A flag, dammit
file1:55:One, two, three, four
file1-56-Get your booty on the dance floor
file1-57-Work it out, shake it, little mama
--
--
file1-61-To work it out; work it, little mama
file1-62-I know you wanna do the Jane Fonda
file1:63:One, two, three, four
file1-64-Get your booty on the dance floor
file1-65-Work it out, shake it, little mama"
  result="$(fish -c 'tbgs around 2 fo')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs fo around 2')"
  [ "$result" = "$expected" ]
}

@test "static around: dynamic overwrites static" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, four
file1:38:One, two, three, four
file1:55:One, two, three, four
file1:63:One, two, three, four"
  result="$(fish -c 'tbgs around 0 fo')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs fo around 0')"
  [ "$result" = "$expected" ]
  expected="file1-15-Mickey, Mickey, fuck me, fuck me
file1-16-More junk in the trunk than a Honda
file1-17-I know you wanna do the Jane Fonda
file1:18:One, two, three, four
file1-19-Get your booty on the dance floor
file1-20-Work it out, shake it, little mama
file1-21-Let me see you do the Jane Fonda
--
--
file1-35-Tasted like chicken and was lemon scented
file1-36-She took me home to her mama
file1-37-I taught them both how to Jane Fonda
file1:38:One, two, three, four
file1-39-Get your booty on the dance floor
file1-40-Work it out, shake it, little mama
file1-41-Let me see you do the Jane Fonda
--
--
file1-52-You could see her ass from outer space
file1-53-So I landed on a planet
file1-54-And planted a Mickey A flag, dammit
file1:55:One, two, three, four
file1-56-Get your booty on the dance floor
file1-57-Work it out, shake it, little mama
file1-58-Let me see you do the Jane Fonda
--
--
file1-60-If you don't know, let me show you how
file1-61-To work it out; work it, little mama
file1-62-I know you wanna do the Jane Fonda
file1:63:One, two, three, four
file1-64-Get your booty on the dance floor
file1-65-Work it out, shake it, little mama
file1-66-Let me see you do the Jane Fonda"
  result="$(fish -c 'tbgs around 3 fo')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs fo around 3')"
  [ "$result" = "$expected" ]
}
