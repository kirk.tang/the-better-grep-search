#!/usr/bin/env bats

@test "static recur: static" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, four
file1:38:One, two, three, four
file1:55:One, two, three, four
file1:63:One, two, three, four
fileDir2/file2:3:for loop that C-based languages
fileDir2/file2:4:  actually foreach loops.
fileDir2/file2:8:problems with for loops in"
  result="$(fish -c 'tbgs fo')"
  [ "$result" = "$expected" ]
}

@test "static recur: when dynamic is the same as static, nothing happens" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, four
file1:38:One, two, three, four
file1:55:One, two, three, four
file1:63:One, two, three, four
fileDir2/file2:3:for loop that C-based languages
fileDir2/file2:4:  actually foreach loops.
fileDir2/file2:8:problems with for loops in"
  result="$(fish -c 'tbgs recur fo')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs fo recur')"
  [ "$result" = "$expected" ]
}

@test "static recur: dynamic overwrites static" {
  cd $BATS_TEST_DIRNAME
  expected="file1:18:One, two, three, four
file1:38:One, two, three, four
file1:55:One, two, three, four
file1:63:One, two, three, four"
  result="$(fish -c 'tbgs no-recur fo')"
  [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs fo no-recur')"
  [ "$result" = "$expected" ]
}
