#!/usr/bin/env bats

@test "filter no-around color case: 2 words" {
  cd $BATS_TEST_DIRNAME
  expected="file11:18:[01;31m[KOne[m[K, [0;32mtwo[0m, three, four
file11:38:[01;31m[KOne[m[K, [0;32mtwo[0m, three, four
file11:55:[01;31m[KOne[m[K, [0;32mtwo[0m, three, four
file11:63:[01;31m[KOne[m[K, [0;32mtwo[0m, three, four"
  result="$(fish -c 'tbgs One two')"; [ "$result" = "$expected" ]
  expected="file11:18:[0;32mOne[0m, [01;31m[Ktwo[m[K, three, four
file11:38:[0;32mOne[0m, [01;31m[Ktwo[m[K, three, four
file11:55:[0;32mOne[0m, [01;31m[Ktwo[m[K, three, four
file11:63:[0;32mOne[0m, [01;31m[Ktwo[m[K, three, four"
  result="$(fish -c 'tbgs two One')"; [ "$result" = "$expected" ]
  expected=""
  result="$(fish -c 'tbgs one two')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two one')"; [ "$result" = "$expected" ]
  expected="file21:3:Her debut single, a cover version of Bon Iver's Skinny Love, was her breakthrough, charting all across Europe and earning platinum certification six times in Australia. Her self-titled debut album, Birdy, was released on 7 November 2011 to similar success, peaking at number [01;31m[Kone[m[K in Australia, Belgium and the Netherlands. Her second studio album, Fire Within, was released on 23 September 2013 in the [0;32mUK[0m. At the 2014 Brit Awards, she was nominated for Best British Female Solo Artist. Her third studio album, Beautiful Lies, was released on 25 March 2016."
  result="$(fish -c 'tbgs one UK')"; [ "$result" = "$expected" ]
  expected="file21:3:Her debut single, a cover version of Bon Iver's Skinny Love, was her breakthrough, charting all across Europe and earning platinum certification six times in Australia. Her self-titled debut album, Birdy, was released on 7 November 2011 to similar success, peaking at number [0;32mone[0m in Australia, Belgium and the Netherlands. Her second studio album, Fire Within, was released on 23 September 2013 in the [01;31m[KUK[m[K. At the 2014 Brit Awards, she was nominated for Best British Female Solo Artist. Her third studio album, Beautiful Lies, was released on 25 March 2016."
  result="$(fish -c 'tbgs UK one')"; [ "$result" = "$expected" ]
  expected=""
  result="$(fish -c 'tbgs one uk')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs uk one')"; [ "$result" = "$expected" ]
}

@test "filter no-around color case: 4 words" {
  cd $BATS_TEST_DIRNAME
  expected="file11:18:[01;31m[KOne[m[K, [0;32mtwo[0m, [0;34mthree[0m, [0;33mfour[0m
file11:38:[01;31m[KOne[m[K, [0;32mtwo[0m, [0;34mthree[0m, [0;33mfour[0m
file11:55:[01;31m[KOne[m[K, [0;32mtwo[0m, [0;34mthree[0m, [0;33mfour[0m
file11:63:[01;31m[KOne[m[K, [0;32mtwo[0m, [0;34mthree[0m, [0;33mfour[0m"
  result="$(fish -c 'tbgs One two four three')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs case One two four three')"; [ "$result" = "$expected" ]
  expected="file11:18:[0;32mOne[0m, [01;31m[Ktwo[m[K, [0;33mthree[0m, [0;34mfour[0m
file11:38:[0;32mOne[0m, [01;31m[Ktwo[m[K, [0;33mthree[0m, [0;34mfour[0m
file11:55:[0;32mOne[0m, [01;31m[Ktwo[m[K, [0;33mthree[0m, [0;34mfour[0m
file11:63:[0;32mOne[0m, [01;31m[Ktwo[m[K, [0;33mthree[0m, [0;34mfour[0m"
  result="$(fish -c 'tbgs two One three four')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two One three four case')"; [ "$result" = "$expected" ]
  expected=""
  result="$(fish -c 'tbgs case one two four three')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs case two one three four')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs one uk four three')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs uk one three uk')"; [ "$result" = "$expected" ]
}

@test "filter no-around color case: 2 words partial match" {
  cd $BATS_TEST_DIRNAME
  expected="file11:18:[01;31m[KOne[m[K, [0;32mtw[0mo, three, four
file11:38:[01;31m[KOne[m[K, [0;32mtw[0mo, three, four
file11:55:[01;31m[KOne[m[K, [0;32mtw[0mo, three, four
file11:63:[01;31m[KOne[m[K, [0;32mtw[0mo, three, four"
  result="$(fish -c 'tbgs One tw')"; [ "$result" = "$expected" ]
  expected="file11:18:[0;32mOn[0me, [01;31m[Ktwo[m[K, three, four
file11:38:[0;32mOn[0me, [01;31m[Ktwo[m[K, three, four
file11:55:[0;32mOn[0me, [01;31m[Ktwo[m[K, three, four
file11:63:[0;32mOn[0me, [01;31m[Ktwo[m[K, three, four"
  expected="file11:18:[0;32mOn[0me, [01;31m[Ktwo[m[K, three, four
file11:38:[0;32mOn[0me, [01;31m[Ktwo[m[K, three, four
file11:55:[0;32mOn[0me, [01;31m[Ktwo[m[K, three, four
file11:63:[0;32mOn[0me, [01;31m[Ktwo[m[K, three, four"
  result="$(fish -c 'tbgs two On')"; [ "$result" = "$expected" ]
  expected="file11:18:One, t[0;32mwo[0m, [01;31m[Kthr[m[Kee, four
file11:38:One, t[0;32mwo[0m, [01;31m[Kthr[m[Kee, four
file11:55:One, t[0;32mwo[0m, [01;31m[Kthr[m[Kee, four
file11:63:One, t[0;32mwo[0m, [01;31m[Kthr[m[Kee, four"
  result="$(fish -c 'tbgs thr wo')"; [ "$result" = "$expected" ]
  expected=""
  result="$(fish -c 'tbgs one tw')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs two on')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs thr wO')"; [ "$result" = "$expected" ]
  expected="file11:18:One, t[0;32mw[0mo, [01;31m[Kthr[m[Kee, four
file11:38:One, t[0;32mw[0mo, [01;31m[Kthr[m[Kee, four
file11:55:One, t[0;32mw[0mo, [01;31m[Kthr[m[Kee, four
file11:63:One, t[0;32mw[0mo, [01;31m[Kthr[m[Kee, four
file21:3:Her debut single, a cover version of Bon Iver's Skinny Love, [0;32mw[0mas her break[01;31m[Kthr[m[Kough, charting all across Europe and earning platinum certification six times in Australia. Her self-titled debut album, Birdy, [0;32mw[0mas released on 7 November 2011 to similar success, peaking at number one in Australia, Belgium and the Netherlands. Her second studio album, Fire Within, [0;32mw[0mas released on 23 September 2013 in the UK. At the 2014 Brit A[0;32mw[0mards, she [0;32mw[0mas nominated for Best British Female Solo Artist. Her third studio album, Beautiful Lies, [0;32mw[0mas released on 25 March 2016."
  result="$(fish -c 'tbgs thr w')"; [ "$result" = "$expected" ]
  expected="file21:3:Her debut single, a cover version of Bon Iver's Skinny Love, was her break[01;31m[Kthr[m[Kough, charting all across Europe and earning platinum certification six times in Australia. Her self-titled debut album, Birdy, was released on 7 November 2011 to similar success, peaking at number one in Australia, Belgium and the Netherlands. Her second studio album, Fire [0;32mW[0mithin, was released on 23 September 2013 in the UK. At the 2014 Brit Awards, she was nominated for Best British Female Solo Artist. Her third studio album, Beautiful Lies, was released on 25 March 2016."
  result="$(fish -c 'tbgs thr W')"; [ "$result" = "$expected" ]
}

@test "filter no-around color case: 2 words word only appears in file path" {
  cd $BATS_TEST_DIRNAME
  expected=""
  result="$(fish -c 'tbgs one file')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs file two')"; [ "$result" = "$expected" ]
  result="$(fish -c 'tbgs One file')"; [ "$result" = "$expected" ]
}
