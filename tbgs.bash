tbgs() {
  python3 $( realpath $( dirname "${BASH_SOURCE[0]}" ) )"/tbgs.py" $@
}

_tbgs_completions()
{
  COMPREPLY=($(compgen -W "no-color color no-case case no-recur recur no-word word around no-dot keyword pipe-to max-length" "${COMP_WORDS[${#COMP_WORDS[@]}-1]}"))
}

complete -F _tbgs_completions tbgs
