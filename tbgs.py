# imports and setups {{{1
import json
import subprocess
import sys
import re
import os
import copy
import random
import string

SYSTEM_CONFIG_FILE_NAME = 'config.json'
USER_CONFIG_FILE_NAME = os.path.expanduser('~/.tbgs-config.json')
LOCAL_CONFIG_FILE_NAME = '.tbgs-config.json'

COLORS = [
    '\033[0;31m', # red
    '\033[0;32m', # green
    '\033[0;33m', # brown/orange
    '\033[0;34m', # blue
    '\033[0;35m', # purple
    '\033[0;36m', # cyan
    '\033[0;37m', # light gray
]
NO_COLOR = '\033[0m'

searchDotFiles = True

def randomStringDigits(stringLength=12):
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))
grepResultFilePath = '/tmp/grep_result_' + randomStringDigits(12)
grepResultVimrcFilePath = '/tmp/grep_result_vimrc' + randomStringDigits(12)

# static config loaders and parsers {{{1
def loadUserConfig():
    try:
        with open(USER_CONFIG_FILE_NAME, 'r') as configFile:
            data = configFile.read()
        return json.loads(data)
    except ValueError as err:
        return {}
    except FileNotFoundError as err:
        return {}

def loadGlobalConfig():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(dir_path + '/' + SYSTEM_CONFIG_FILE_NAME, 'r') as configFile:
        data = configFile.read()
    try:
        return json.loads(data)
    except ValueError as err:
        print("config.json file wrong format:\n" + str(err))
        quit()

def loadLocalConfigs():
    directories = os.getcwd().split(r'/')
    localConfigs = []
    for r in range(len(directories), 0, -1):
        currentPath = '/'.join(directories[:r]) or '/'
        try:
            with open(currentPath + '/' + LOCAL_CONFIG_FILE_NAME, 'r') as localConfigFile:
                data = localConfigFile.read()
            localConfigs.insert(0, json.loads(data))
        except:
            continue
    return localConfigs

def merge(source, destination):
    for key, value in source.items():
        if isinstance(value, dict):
            # get node or create one
            node = destination.setdefault(key, {})
            merge(value, node)
        else:
            destination[key] = value
    return destination

def mergeObjects(objects):
    mergedObject = copy.deepcopy(objects[0])
    for obj in objects:
        mergedObject = merge(obj, mergedObject)
    return mergedObject

    return destination

def cleanUpConfig(config):
    cleanedUpConfig = copy.deepcopy(config)
    if (cleanedUpConfig['pipeTo'] == 'vim'):
        cleanedUpConfig['colorOutput'] = False
    return cleanedUpConfig

def getStaticConfig():
    configs = loadLocalConfigs()
    configs.insert(0, loadUserConfig())
    configs.insert(0, loadGlobalConfig())
    return mergeObjects(configs)

def formatArgumentsPerMaps(arguments, maps):
    for key in maps:
        matches = [mapIndex for mapIndex, argument in enumerate(arguments) if argument == key]
        if (len(matches) == 0):
            continue
        for mapIndex in reversed(matches):
            if (mapIndex > 0 and arguments[mapIndex - 1] == 'keyword'):
                continue
            else:
                del arguments[mapIndex]
                for segmentIndex in range(len(maps[key].split())):
                    arguments.insert(mapIndex + segmentIndex, maps[key].split()[segmentIndex])
    return arguments

# dynamic config parsers {{{1
def parseArgv():
    arguments = copy.copy(sys.argv[1:])
    userConfigs = loadLocalConfigs()
    userConfigs.insert(0, loadUserConfig())
    userConfigs = mergeObjects(userConfigs)
    try:
        arguments = formatArgumentsPerMaps(arguments, userConfigs['maps'])
    except:
        arguments = arguments
    dynamicConfig = {}
    searchWords = []
    skipOne = False
    for index, argument in enumerate(arguments):
        if (skipOne):
            skipOne = False
            continue
        if (argument == 'case'):
            dynamicConfig['caseSensitive'] = True
        elif (argument == 'no-case'):
            dynamicConfig['caseSensitive'] = False
        elif (argument == 'color'):
            dynamicConfig['colorOutput'] = True
        elif (argument == 'no-color'):
            dynamicConfig['colorOutput'] = False
        elif (argument == 'recur'):
            dynamicConfig['recursive'] = True
        elif (argument == 'no-recur'):
            dynamicConfig['recursive'] = False
        elif (argument == 'no-dot'):
            global searchDotFiles
            searchDotFiles = False
        elif (argument == 'word'):
            dynamicConfig['word'] = True
        elif (argument == 'no-word'):
            dynamicConfig['word'] = False
        elif (argument == 'around'):
            dynamicConfig['around'] = arguments[index + 1]
            skipOne = True
        elif (argument == 'max-length'):
            dynamicConfig['maxLength'] = arguments[index + 1]
            skipOne = True
        elif (argument == 'keyword'):
            searchWords.append(arguments[index + 1])
            skipOne = True
        elif (argument == 'pipe-to'):
            dynamicConfig['pipeTo'] = arguments[index + 1]
            if (arguments[index + 1] != 'less' and arguments[index + 1] != 'none'):
                dynamicConfig['colorOutput'] = False
            skipOne = True
        else:
            searchWords.append(argument)
    if (len(searchWords) == 0):
        print("There is no search word to search!")
        quit()
    if (pipeTo in dynamicConfig and dynamicConfig['pipeTo'] != 'none' and dynamicConfig['pipeTo'] != 'less'):
        dynamicConfig['colorOutput'] = False
    return (dynamicConfig, searchWords)

# grep params generators {{{1
def getCaseOptionString(config):
    return ("-i", "")[config['caseSensitive']]
def getRecurOptionString(config):
    return ("", "-r")[config['recursive']]
def getPatternString(config):
    return ("*", "* .[^.]*")[searchDotFiles]
def getWordOptionString(config):
    return ("", "-w")[config['word']]
def getColorOptionString(config):
    return '--color=' + ("never", "always")[config['colorOutput']]
def getAroundString(config):
    around = config['around']
    return ("", f"-C {around}")[bool(int(around))]
def getExcludeDirsString(config):
    excludeDirs = config['excludeDirs']
    if (not excludeDirs or len(excludeDirs) == 0):
        return ''
    result = " --exclude-dir="
    if (len(excludeDirs) == 1):
        result += excludeDirs[0]
    else:
        result += '{'
        for index, excludeDir in enumerate(excludeDirs):
            result += excludeDir
            if (index < len(excludeDirs) - 1):
                result += ','
        result += '}'
    return result
def getExcludeFilesString(config):
    excludeFiles = config['excludeFiles']
    if (not excludeFiles or len(excludeFiles) == 0):
        return ''
    result = " --exclude="
    if (len(excludeFiles) == 1):
        result += excludeFiles[0]
    else:
        result += '{'
        for index, excludeFile in enumerate(excludeFiles):
            result += excludeFile
            if (index < len(excludeFiles) - 1):
                result += ','
        result += '}'
    return result

# apply color to string
def applyColorToWord(searchWord, index):
    if (index >= len(COLORS)):
        index = 0
    return COLORS[index] + searchWord + NO_COLOR

def applyColorToContent(searchWord, content, caseSensitive, index):
    regex = (re.compile(searchWord, re.I), re.compile(searchWord))[caseSensitive]
    matches = regex.findall(content)
    splits = regex.split(content)
    result = ''
    for i in range(len(matches)):
        result += splits[i] + applyColorToWord(matches[i], index)
    result += splits[-1]
    return result

def applyColorToSection(searchWord, lineNumberString, contentString, caseSensitive, index):
    lineNumberLines = lineNumberString.split('\n')
    contentStringLines = applyColorToContent(searchWord, contentString, caseSensitive, index).split('\n')
    result = ''
    for i in range(len(lineNumberLines)):
        result += lineNumberLines[i] + contentStringLines[i] + '\n'
    return result

# filters {{{1
def filterByLength(grepResultString, maxLength):
    lines = grepResultString.split('\n')
    result = []
    for line in lines:
        if len(line.strip()) == 0:
            continue
        parts = line.strip().split(':', 2)
        if len(parts) != 3:
            parts = line.strip().split('-', 2)
            if len(parts) != 3 and len(line) <= maxLength:
                result.append(line)
            elif len(parts) == 3 and len(parts[2]) <= maxLength:
                result.append(line)
            continue
        if len(parts) == 3 and len(parts[2]) <= maxLength:
            result.append(line)
    return '\n'.join(result)

def filterCurrentLine(grepResult, config, searchWords, index):
    caseSensitive = config['caseSensitive']
    colorOutput = config['colorOutput']
    def forEverySearchWord(lines, searchWords, index):
        if (len(searchWords) == 0):
            return lines
        searchWord = searchWords[0]
        result = []
        for line in lines:
            if (len(line) == 0):
                continue
            contents = line.split(':', 2)
            if ((caseSensitive and searchWord in contents[2]) or
                ((not caseSensitive) and searchWord.lower() in contents[2].lower())):
                    if colorOutput:
                        result.append(contents[0] + ':' + contents[1] + ':' + applyColorToContent(searchWord, contents[2], caseSensitive, index))
                    else:
                        result.append(line)
        return forEverySearchWord(result, searchWords[1:], index + 1)
    lines = grepResult.split('\n')
    result = forEverySearchWord(lines, searchWords, index)
    return '\n'.join(result)

def filterAroundLine(grepResult, config, searchWords, firstSearchWord, index):
    caseSensitive = config['caseSensitive']
    colorOutput = config['colorOutput']
    def forEverySearchWord(sections, searchWords, index):
        if (len(searchWords) == 0):
            return sections
        searchWord = searchWords[0]
        def getRegexSearchEndIndex(regex, string):
            try:
                return regex.search(string).end()
            except:
                return len(string)
        def getContentStartIndex(line):
            return min(getRegexSearchEndIndex(re.compile(r':\d+:'), line),
                getRegexSearchEndIndex(re.compile(r'-\d+-'), line))
        filteredSections = []
        for section in sections:
            lines = section.split('\n')
            contentString = ''
            lineNumberString = ''
            for line in lines:
                contentString += line[getContentStartIndex(line):]
                contentString += '\n'
                lineNumberString += line[0:getContentStartIndex(line)]
                lineNumberString += '\n'
            if ((caseSensitive and searchWord in contentString) or
                (not caseSensitive and searchWord.lower() in contentString.lower())):
                if (colorOutput):
                    filteredSections.append(applyColorToSection(searchWord, lineNumberString, contentString, caseSensitive, index).strip())
                else:
                    filteredSections.append(section.strip())
        return forEverySearchWord(filteredSections, searchWords[1:], index + 1)
    sections = grepResult.split('\n--\n')
    result = forEverySearchWord(sections, searchWords, index)
    return '\n--\n'.join(result)

# pipe to {{{1
def pipeTo(grepResultString, config, searchWord):
    if int(config['maxLength']) > 0:
        grepResultString = filterByLength(grepResultString, int(config['maxLength']))
    if config['pipeTo'] == 'none':
        print(grepResultString)
    elif config['pipeTo'] == 'less':
        with open(grepResultFilePath, 'w') as grepResultFile:
            grepResultFile.write(grepResultString)
        subprocess.call(['less', '-FRX', grepResultFilePath])
    else:
        if config['pipeTo'] == 'code':
            config['pipeTo'] = 'Visual Studio Code'
        elif config['pipeTo'] == 'chrome':
            config['pipeTo'] = 'Google Chrome'
        elif config['pipeTo'] == 'canary':
            config['pipeTo'] = 'Google Chrome Canary'
        elif config['pipeTo'] == 'mvim':
            config['pipeTo'] = 'MacVim'
        elif config['pipeTo'] == 'subl':
            config['pipeTo'] = 'Sublime Text'
        with open(grepResultFilePath, 'w') as grepResultFile:
            grepResultFile.write(grepResultString)
        with open(grepResultVimrcFilePath, 'w') as grepResultVimrcFile:
            grepResultVimrcFile.write('silent! filetype plugin on\n')
            grepResultVimrcFile.write('silent! syntax enable\n')
            grepResultVimrcFile.write('silent! set hlsearch\n')
            grepResultVimrcFile.write('silent! set nobackup\n')
            grepResultVimrcFile.write('silent! set noswapfile\n')
            grepResultVimrcFile.write('silent! set nomodeline\n')
            grepResultVimrcFile.write('silent! set ignorecase\n')
            grepResultVimrcFile.write('silent! set smartcase\n')
            grepResultVimrcFile.write('silent! g/^--$/normal 0d$\n')
            grepResultVimrcFile.write('silent! %s/' + searchWord + '//gn\n')
            grepResultVimrcFile.write('silent! normal 0gg\n')
            if config['pipeTo'] != 'vim':
                grepResultVimrcFile.write('function! OpenAllFilesInVimFunction()\n')
                grepResultVimrcFile.write('  %s/^\\([^:]*\\):\\d*:.*$/"\\1" \\\\/\n')
                grepResultVimrcFile.write('  sort u\n')
                grepResultVimrcFile.write('  $norm $x\n')
                grepResultVimrcFile.write('  0norm Iopen -a "' + config['pipeTo'] + '" \n')
                grepResultVimrcFile.write('  %!sh\n')
                grepResultVimrcFile.write('  normal u\n')
                grepResultVimrcFile.write('endfunction\n')
                grepResultVimrcFile.write('nnoremap <space>O :silent! call OpenAllFilesInVimFunction()<cr>\n')
                grepResultVimrcFile.write('function! OpenCurrentFileInVimFunction()\n')
                grepResultVimrcFile.write('  s/^\\([^:]*\\):\\d*:.*$/"\\1"/\n')
                grepResultVimrcFile.write('  norm Iopen -a "' + config['pipeTo'] + '" \n')
                grepResultVimrcFile.write('  .!sh\n')
                grepResultVimrcFile.write('  normal u\n')
                grepResultVimrcFile.write('endfunction\n')
                grepResultVimrcFile.write('nnoremap <space>o :silent! call OpenCurrentFileInVimFunction()<cr>\n')

        dir_path = os.path.dirname(os.path.realpath(__file__))
        subprocessCallArgs = [
            '/Applications/MacVim.app/Contents/MacOS/Vim',
            grepResultFilePath,
            '-c', 'so ' + grepResultVimrcFilePath,
        ]
        if config['pipeTo'] == 'vim':
            subprocessCallArgs.append('-c')
            subprocessCallArgs.append(':so ' + dir_path + '/pipe-to-vim.vim')
        try:
            subprocess.call(subprocessCallArgs)
        except:
            subprocessCallArgs[0] = 'vim'
            subprocess.call(subprocessCallArgs)

# runner {{{1
def main():
    (dynamicConfig, searchWords) = parseArgv()
    config = cleanUpConfig(mergeObjects([getStaticConfig(), dynamicConfig]))
    try:
        subprocess.call(['rm', '-rf', grepResultFilePath])
        subprocess.call(['rm', '-rf', grepResultVimrcFilePath])
        subprocess.call([
            'bash',
            '-c',
            f"grep -nIEs {getRecurOptionString(config)} {getCaseOptionString(config)}" +
            f" {getColorOptionString(config)} {getAroundString(config)} {getWordOptionString(config)}" +
            f" {getExcludeDirsString(config)} {getExcludeFilesString(config)}" +
            f" {searchWords[0]} {getPatternString(config)}" +
            f" > {grepResultFilePath}"
        ])
        with open(grepResultFilePath, 'r') as grepResultFile:
            grepResult = grepResultFile.read()
    except:
        quit()
    if (len(searchWords) == 1):
        pipeTo(grepResult, config, searchWords[0])
    elif (not config['around']):
        pipeTo(filterCurrentLine(grepResult, config, searchWords[1:], 1), config, searchWords[0])
    else:
        pipeTo(filterAroundLine(grepResult, config, searchWords[1:], searchWords[0], 1), config, searchWords[0])

main()
