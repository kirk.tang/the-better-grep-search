# {VERSION} {DATE}

# 1.2.0 2020/05/02
- custom pipe-to: allow piping to external programs
- static config json file: pipeTo should be set to none if no piping is needed, empty string ""
  won't work any more

# 1.1.1 2020/04/29
- better default settings in vimrc

# 1.1.0 2020/04/28
- add support to user configuration file
- stop exposing no-dot configuration
- fix tests

# 1.0.0 2020/04/27
- Initial launch
