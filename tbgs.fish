function tbgs
  python3 (realpath (dirname (status filename)))"/tbgs.py" $argv
end

complete -c tbgs -f \
  -a 'no-color color no-case case no-recur recur no-word word around no-dot keyword pipe-to max-length'
