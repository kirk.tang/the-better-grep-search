# install tbgs
function install_tbgs_fish
  echo "source "(realpath (dirname (status filename)))"/tbgs.fish"  >> ~/.config/fish/config.fish
  echo "tbgs installation done, please start a new fish session by running `fish`"
end
# only run tbgs installation when tbgs is not defined
type tbgs > /dev/null 2>&1; and echo 'You already have tgbs, cheers!'; or install_tbgs_fish
